#!/bin/owls.wiki.losu.tech

# owls · wiki服务黑魔法
## OWLS可以做什么？
+ OWLS是一个基于洛书实现的wiki建站服务工具，可以让您利用markdown文档生成精美的web网页
## OWLS有哪些特色？
+ 更轻量、快速的选择
    - 基于高效的 losu script，完全由其标准库书写，仅需数十KB的资源需求
    - 支持多种markdown引擎
    - 通过洛书模块工具一键部署
+ 满足个性化需求
    - 原生html模块支持
    - 支持工程自定义模板
+ 无中间文件
    - 无html输出生成
    - 适合git版本管理机制
    - 保持源码整洁度
+ 开源开放便捷
    - MIT LICENSE 许可
    - 支持 lpt 一键安装
    - 自由地修改与分发


## 附录
```
本站采用 marked.js 提供MarkDown渲染服务
    * marked v6.0.0 - a markdown parser
    * Copyright (c) 2011-2023, Christopher Jeffrey. (MIT Licensed)
    * https://github.com/markedjs/marked

owls classic主题
```