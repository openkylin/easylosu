#!/bin/owls.wiki.losu.tech
# stdlib 
+ stdlib 是洛书的标准库，由 IO ，字节，文件，数学，字符串，系统，多线程，时间，虚拟机等多个模块组合而成

```python
import "stdlib"

```

相当于

```python
import "std"
import "stdbyte"
import "stdfile"
import "stdmath"
import "stdstring"
import "stdsys"
import "stdthread"
import "stdtime"
import "stdvm"

```